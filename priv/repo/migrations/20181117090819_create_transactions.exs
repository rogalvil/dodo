defmodule Dodo.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :float
      add :card_number, :string
      add :fullname, :string
      add :card_type, :string
      add :expirate_date, :string
      add :cvv, :string
      add :last_digits, :string
      add :status, :string
      add :authorization_code, :string

      timestamps()
    end

  end
end
