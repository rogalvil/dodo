defmodule DodoWeb.PageController do
  use DodoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
