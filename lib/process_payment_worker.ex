defmodule ProcessPaymentWorker do
  def perform(id) do
    alias Dodo.{Repo, Transaction}
    #hacer algun otro proceso
    Ecto.Changeset.change( Repo.get_by(Transaction, id: id), %{status: "done", authorization_code: Generator.randstring(64)}) |> Repo.update()
    IO.puts("ProcessPaymentWorker called with id: #{id}")
  end
end