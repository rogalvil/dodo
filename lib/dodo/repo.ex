defmodule Dodo.Repo do
  use Ecto.Repo,
    otp_app: :dodo,
    adapter: Ecto.Adapters.Postgres
end
