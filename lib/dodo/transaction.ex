defmodule Dodo.Transaction do
  use Ecto.Schema
  import Ecto.Changeset


  schema "transactions" do
    field :amount, :float
    field :authorization_code, :string
    field :card_number, :string
    field :card_type, :string
    field :cvv, :string
    field :expirate_date, :string
    field :fullname, :string
    field :last_digits, :string
    field :status, :string

    #timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:amount, :card_number, :fullname, :card_type, :expirate_date, :cvv, :last_digits, :status, :authorization_code])
    |> validate_required([:amount, :card_number, :fullname, :card_type, :expirate_date, :cvv, :last_digits, :status, :authorization_code])
  end
end
